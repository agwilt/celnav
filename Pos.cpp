#include "Pos.hpp"

#include <cmath>
#include "base.hpp"
#include "util.hpp"

namespace celnav {

Pos Pos::horizontal_coordinates_of(Pos const& other_pos) const
{
	Angle const lha = (other_pos.lon - lon).normalise();
	double const cos_alt = sin(lat) * sin(other_pos.lat) + cos(lat) * cos(other_pos.lat) * cos(lha);
	Pos ans;
    ans.lat = Angle::radians(acos(cos_alt));
	ans.lon = Angle::radians(acos((sin(other_pos.lat) - sin(ans.lat)*sin(lat)) / (cos_alt * cos(lat))));
	if (lha < 180_deg) {
		ans.lon = 360_deg - ans.lon;
	}
	return ans;
}

} // namespace celnav

std::ostream &operator<<(std::ostream &stream, celnav::Pos const pos)
{
    using namespace celnav;
    return stream << pos.lat.abs().to_string(AngleFormat::DDM) << (pos.lat >= 0 ? 'N' : 'S') << " "
                  << pos.lon.abs().to_string(AngleFormat::DDM) << (pos.lon >= 0 ? 'E' : 'W');
}
