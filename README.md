### Current Features
 * Altitude correction from sextant measurement to geocentric altitude
 * Averaging of multiple sights
 * Computed altitude, azimuth of celestial body as seen from Assumed Position

### Planned Features
 * User interfaces to be able to use all this:
    - [ ] command-line shell
    - [ ] TUI, usable on LCD?
    - [ ] GUI, maybe Qt based
    - [ ] python module
 * Automatic fix from multiple position lines
 * DR tools, to enable automatic running fixes
 * Custom, non-celestial LOPs
