#ifndef CEL_NAV_SHELL_H
#define CEL_NAV_SHELL_H

#include <string>

#include "System.hpp"

namespace celnav {

class CelNavShell {
public:
	void run();

private:
	std::string prompt() const;
	constexpr auto const& arg(unsigned const i) const { return _tokens.at(1 + i); }

	void select();

private:
	celnav::System _system;
	celnav::Instance *_cur_instance = nullptr;

	std::vector<std::string> _tokens;
};

}

#endif // CEL_NAV_SHELL_H
