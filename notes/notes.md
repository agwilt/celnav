# CelNav Programs:

## Interface:
 * readline-based command line?
 * separate command line utils?
 * python library?
 * GUI?

## Single-use celnav tools:
 * GHA, Dec of celestial body at given time
 * Hc, Zn of celestial body (or any GP?), given AP
 * Great circle distance (this applies to goegraphic as well as horizontal positions, of course. hmm)
 * Apparent distance between given pair of stars
 * St. Hilaire intercept --> ΔH, Zn
 * (Complete lunar procedure?)
 * (Averaging of sights?)
 * Ideally: builtin ephemerides

## DR Helpers?
 * Enter time, course, speed since last update (NOTE: course will want to be corrected if compass)
 * Or define new position, along with course&speed
 * Enter leeeway/tide stuff
 * Update EP with single LOP?
How to calculate in speed? If using "continuous" log, always enter past *distance*. Else projected speed for
next bit?

## Plotting:
Always rectilinear projection? Then can use the builtin pointer function to
mark specific coordinates.


# Advanced, automated celnav:
Given (long) list of measured star positions and exact time, determine all star
identities?
Get star magnitudes as well?


# Non-electronic tables to have:
 * EOT
 * Sun Dec
 * Polaris altitude correction table
 * sin approximation:
   - sin(x°) ≈ x/60 for x∈[0,30°]
   - sin(x°) ≈ 1 - ((90-x)/60)²/2 for x∈[30°,90°]
   - i.e. cos(x°) ≈ 1 - (x/60)²/2 for x∈[0°,60°]


# General Thoughts:
How important is the exact horizon if given a large FOV?
--> All that matters is to have the same error in each direction, then we can
find the zenith correctly and work from there ...
