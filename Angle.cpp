#include "Angle.hpp"

#include <format>
#include <iostream>

namespace celnav {

std::string Angle::dd_string() const
{
    return std::format("{:.3f}", to_degrees());
}

std::string Angle::ddm_string() const
{
    int const angle_deciminutes = std::round(to_minutes() * 10.0);
    auto const [angle_minutes, decimal] = std::div(angle_deciminutes, 10);
    auto const [degrees, minutes] = std::div(angle_minutes, 60);
    std::string s = "";
    if (_angle < 0) s += "-";
    s += std::format("{:d}°{:d}.{:d}'", degrees, minutes, decimal);
    return s;
}

std::string Angle::dms_string() const
{
    int const angle_seconds = std::round(to_seconds());
    auto const [angle_minutes, seconds] = std::div(angle_seconds, 60);
    auto const [degrees, minutes] = std::div(angle_minutes, 60);
    std::string s = "";
    if (_angle < 0) s += "-";
    s += std::format("{:d}°{:d}'{:d}\"", degrees, minutes, seconds);
    return s;
}

std::string Angle::to_string(AngleFormat const format) const
{
    switch (format) {
        case AngleFormat::DD: return dd_string();
        case AngleFormat::DDM: return ddm_string();
        case AngleFormat::DMS: return dms_string();
        default:
			std::cerr << "Error: Invalid angle format\n";
			exit(1);
    }
}

Angle Angle::from_stdin()
{
	std::string line;
	while (true) {
		std::getline(std::cin, line);
		if (auto const maybe_angle = from_string(line)) {
			return *maybe_angle;
		}
	}
}

std::optional<Angle> Angle::from_string(std::string const& dms)
{
	double d,m,s;
	d = m = s = 0.0;
	if (std::sscanf(dms.c_str(), "%lf°%lf'%lf", &d, &m, &s) < 1) {
		std::cerr << "Please give angle in format \"deg°[min'[sec\"]]\"\n";
		return std::nullopt;
	}
	auto ans = degrees(std::abs(d) + m/60 + s/3600);
    return dms[0] == '-' ? -ans : ans;
}

} // namespace celnav
