#ifndef ANGLE_H
#define ANGLE_H

#include <cmath>
#include <string>
#include <optional>

namespace celnav {

enum class AngleFormat {
	DD, // decimal degrees
	DDM, // degrees & decimal minutes
	DMS // degrees, minutes, seconds
};

class Angle;
constexpr Angle operator ""_deg(unsigned long long deg);

class Angle {
public:
    constexpr explicit Angle() : _angle{0} {}
    constexpr static Angle radians(double const rad) { return Angle(rad); }
    constexpr static Angle degrees(double const deg) { return Angle(deg * (std::numbers::pi / 180)); }
    constexpr static Angle minutes(double const min) { return degrees(min / 60.0); }
    static std::optional<Angle> from_string(std::string const& dms); // format: "deg°[min'[sec"]]"
    static Angle from_stdin();

    constexpr operator double() const { return to_radians(); } // radians by default, for trig functions
    constexpr double to_radians() const { return _angle; }
    constexpr double to_degrees() const { return _angle * 180 / std::numbers::pi; }
    constexpr int deg_trunc() const { return std::trunc(to_degrees());}
    constexpr double to_minutes() const { return to_degrees() * 60; }
    constexpr double to_seconds() const { return to_minutes() * 60; }

    std::string to_string(AngleFormat format = AngleFormat::DDM) const;
    std::string dd_string() const;
    std::string ddm_string() const;
    std::string dms_string() const;

    constexpr Angle & normalise() { _angle = fmod(_angle, 360_deg); return *this; }
    constexpr Angle abs() const { return Angle(std::abs(_angle)); }

    constexpr Angle operator-() const { return Angle(-_angle); }
    constexpr Angle operator+(Angle const rhs) const { return Angle(_angle + rhs._angle); }
    constexpr Angle operator-(Angle const rhs) const { return Angle(_angle - rhs._angle); }
    constexpr Angle const& operator+=(Angle const rhs) { _angle += rhs._angle; return *this; }
    constexpr Angle const& operator-=(Angle const rhs) { _angle -= rhs._angle; return *this; }
    constexpr Angle const& operator*=(double const rhs) { _angle *= rhs; return *this; }
    constexpr Angle const& operator/=(double const rhs) { _angle /= rhs; return *this; }

private:
    constexpr explicit Angle(double const angle_rad) : _angle{angle_rad} {}

private:
    double _angle;
};

constexpr Angle operator*(double const lhs, Angle const rhs)
{
    return Angle::radians(lhs * rhs.to_radians());
}

constexpr Angle operator ""_deg(unsigned long long const deg)
{
    return Angle::degrees(deg);
}

} // namespace celnav

#endif // ANGLE_H
