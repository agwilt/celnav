#ifndef LINE_OF_POSITION_H
#define LINE_OF_POSITION_H

#include "Pos.hpp"
#include "base.hpp"

namespace celnav {

struct LineOfPosition {
	Pos geographic_position;
	double observed_altitude;
	TimePoint time;
};

}

#endif // LINE_OF_POSITION_H
