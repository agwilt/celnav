#include "System.hpp"

namespace celnav {

Instance & System::add_instance(std::string const& name)
{
	return _instances.emplace_back(name);
}

Instance & System::copy_instance(size_t const i)
{
	return _instances.emplace_back(_instances.at(i));
}

void System::remove_instance(size_t const i)
{
	_instances.erase(_instances.begin() + i);
}

} // namespace celnav
