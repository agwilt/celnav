#ifndef POS_H
#define POS_H

#include <ostream>
#include "Angle.hpp"

namespace celnav {

struct Pos {
public:
	Pos horizontal_coordinates_of(Pos const& other_pos) const;

public:
	Angle lat; // altitude if being used as horizontal coordinate
	Angle lon; // azimuth if being used as horizontal coordinate
};

} // namespace celnav

std::ostream &operator<<(std::ostream &stream, celnav::Pos pos); // TODO: allow different angle formats

#endif // POS_H
