#include "Instance.hpp"

namespace celnav {

void Instance::update()
{
	if (auto const lop_or_error = sight.process()) {
		_computed_lop = lop_or_error.value();
	}
	if (assumed_position and sight.geographic_position()) {
		_computed_horizontal_coordinates = assumed_position->horizontal_coordinates_of(
			sight.geographic_position().value()
		);
	}
}

Instance::Instance(std::string const& name) : Instance{}
{
	sight.name = name;
}

std::string Instance::one_line_description() const
{
	std::string description = "\"" + name() + "\", ";
	description += [&]() -> std::string {
		switch (sight.sights.size()) {
			case 0: return "no sights";
			case 1: return "single sight";
			default: return "average over " + std::to_string(sight.sights.size()) + " sights";
		}
	}();
	return description;
}

} // namespace celnav
