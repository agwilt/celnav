#ifndef CELNAV_SYSTEM_H
#define CELNAV_SYSTEM_H

#include <vector>
#include "Instance.hpp"

namespace celnav {

class System {
public:
	constexpr System() : _instances{} {}

	Instance & add_instance(std::string const& name);
	Instance & copy_instance(size_t i);
	void remove_instance(size_t i);

	constexpr auto const& instances() const { return _instances; }
	constexpr auto &non_const_instance(size_t const i) { return _instances.at(i); }

private:
	std::vector<Instance> _instances;
};

}

#endif // CELNAV_SYSTEM_H
