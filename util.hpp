#ifndef UTIL_H
#define UTIL_H

#include <numbers>
#include <chrono>
#include "base.hpp"
#include "Pos.hpp"

namespace celnav {

constexpr void assert(bool const test)
{
	if (not test) { exit(1); }
}

// TODO: Same for temperatures
constexpr double kelvin_to_celcius(double const temp_kelvin) { return temp_kelvin - 273.15; }
constexpr double celsius_to_kelvin(double const temp_celsius) { return temp_celsius + 273.15; }

TimePoint date_from_stdin();
std::optional<TimePoint> date_from(std::string const& utc_date);

TimePoint time_from_stdin(TimePoint const& date);
std::optional<TimePoint> time_from(TimePoint const& date, std::string const& time_string);

Pos pos_from_stdin(); // format: "d°m's"N|S d°m's"E|W
std::optional<Pos> pos_from(std::string const& lat, std::string const& lon);

Limb limb_from_stdin();
std::optional<Limb> limb_from(std::string const& s);

}

#endif // UTIL_H
