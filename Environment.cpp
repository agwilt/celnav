#include "Environment.hpp"
#include "util.hpp"
#include <cmath>

namespace celnav {

Angle Environment::alt_corr(
	Angle alt,
	Limb const limb,
	Angle const horizontal_parallax,
	Angle const semidiameter
) const
{
	alt += index_error;
	alt -= Angle::minutes(1.76 * sqrt(height_of_eye)); // dip
	alt -= Angle::minutes(
        (pres_hpascal / 1010.0) * (283.15 / temp_kelvin)
        / tan(alt + Angle::degrees(7.31/(alt.to_degrees() + 4.4)))
    ); // refraction
	alt += cos(alt) * horizontal_parallax; // parallax
	switch (limb) {
		case Limb::LOWER: return alt + semidiameter;
		case Limb::CENTRE: return alt;
		case Limb::UPPER: return alt - semidiameter;
		default: exit(1);
	}
}

} // namespace celnav
