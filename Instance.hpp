#ifndef CELNAV_INSTANCE_H
#define CELNAV_INSTANCE_H

#include "Pos.hpp"
#include "LineOfPosition.hpp"
#include "Sight.hpp"

namespace celnav {

/* GUI idea: have each Instance as a tab, copy over values when making new tab */
class Instance {
public:
	Instance() : sight{}, assumed_position{}, _computed_horizontal_coordinates{}, _computed_lop{} {}
	explicit Instance(std::string const& name);
	void update();

	constexpr auto const& name() const { return sight.name; }
	std::string one_line_description() const; // TODO: probably better just to give Instance a stream ...

public:
	Sight sight;

	std::optional<Pos> assumed_position;
private:
	std::optional<Pos> _computed_horizontal_coordinates;

	std::optional<LineOfPosition> _computed_lop;
};

} // namespace celnav

#endif // CELNAV_INSTANCE_H
