#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include "base.hpp"
#include "util.hpp"
#include "Angle.hpp"

namespace celnav {

struct Environment {
	// TODO: Interpolating function for sextant errors -- but then probably store persistently
	Angle index_error = 0_deg;
	double height_of_eye = 0; // in meters
	double temp_kelvin = celsius_to_kelvin(10);
	double pres_hpascal = 1010;

	Angle alt_corr(Angle sextant_alt, Limb limb, Angle horizontal_parallax, Angle semidiameter) const;
};

} // namespace celnav

#endif // ENVIRONMENT_H
