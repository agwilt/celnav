#include "CelNavShell.hpp"

#include "util.hpp"

#include <iostream>
#include <sstream>
#include <iterator>

extern "C" {
#include <readline/readline.h>
#include <readline/history.h>
}

namespace celnav {

std::string CelNavShell::prompt() const
{
	if (_cur_instance) {
		return "[" + _cur_instance->name() + "] ";
	} else {
		return "> ";
	}
}

void CelNavShell::run()
{
	using namespace std::string_literals;

	for (char *buf; (buf = readline(prompt().c_str())) != nullptr; free(buf)) {
		std::istringstream line{buf};
		_tokens = std::vector<std::string>{
			std::istream_iterator<std::string>{line},
			std::istream_iterator<std::string>{}
		};

		if (_tokens.empty())
			continue;
		auto const num_args = _tokens.size() - 1;

		add_history(buf);

		auto const& command = _tokens[0];

		if (_cur_instance) {
            auto &sight = _cur_instance->sight;
			if (command[0] == 'h') {
				std::cout << "Commands:\n";
				std::cout << " r[eselect] [i]: select LOP number i (leave empty to deselect)\n";
                std::cout << " i[info]: print information about current LOP\n";
				std::cout << " s[et] ap|name|hp|sd|hoe|temp|pres|ie|body|gp [value]\n";
				std::cout << " l[ist]: list single sights\n";
				std::cout << " d[elete] i: delete single sight i\n";
				std::cout << " a[dd] [hh:mm:ss [deg°[min'[sec\"]] [l|c|u]]]: add single sight i\n";
			} else if (command[0] == 'r') {
				select();
            } else if (command[0] == 'i') {
                std::cout << "== " << _cur_instance->one_line_description() << " ==\n";
                std::cout << "Body: " << to_string(sight.body()) << "\n";
                std::cout << "Horizontal Parallax: ";
                if (sight.hp()) std::cout << sight.hp()->to_minutes() << "'"; else std::cout << "-";
                std::cout << "\n";
                std::cout << "Semidiameter: ";
                if (sight.sd()) std::cout << sight.sd()->to_minutes() << "'"; else std::cout << "-";
                std::cout << "\n";
                std::cout << "Date: " << std::chrono::round<std::chrono::days>(sight.date) << "\n";
                std::cout << "Environment:\n";
                std::cout << "   index error = " << sight.environment.index_error.to_minutes() << "'\n";
                std::cout << " height of eye = " << sight.environment.height_of_eye << " m\n";
                std::cout << "   temperature = " << kelvin_to_celcius(sight.environment.temp_kelvin)
                          << " °C\n";
                std::cout << "      pressure = " << sight.environment.pres_hpascal << " hPa\n";
                std::cout << "Geographical Position: ";
                if (sight.geographic_position()) std::cout << *sight.geographic_position();
                else std::cout << "-";
                std::cout << "\n";
                std::cout << "Assumed Position: ";
                if (_cur_instance->assumed_position) std::cout << *_cur_instance->assumed_position;
                else std::cout << "-";
                std::cout << "\n";
			} else if (command[0] == 's') {
				if (num_args < 1) {
					std::cerr << "Error: Please specify value to be [un]set\n";
					continue;
				}
				if (arg(0) == "ap") {
					if (num_args == 3) {
						_cur_instance->assumed_position = pos_from(arg(1), arg(2));
					} else if (num_args == 1) {
						_cur_instance->assumed_position = pos_from_stdin();
					} else {
						std::cerr << "Usage: set ap [DD°MM'SS\"N|S DD°MM'SS\"E|W]\n";
						continue;
					}
                } else if (arg(0) == "gp") {
					if (num_args == 3) {
                        if (auto const maybe_pos = pos_from(arg(1), arg(2))) {
                            sight.set_gp(*maybe_pos);
                        } else {
                            std::cerr << "Usage: set gp [DD°MM'SS\"N|S DD°MM'SS\"E|W]\n";
                            continue;
                        }
					} else if (num_args == 1) {
						sight.set_gp(pos_from_stdin());
					} else {
						std::cerr << "Usage: set gp [DD°MM'SS\"N|S DD°MM'SS\"E|W]\n";
						continue;
					}
                } else if (arg(0) == "name") {
                    if (num_args == 2) {
                        sight.name = arg(1);
                    } else if (num_args == 1) {
                        std::cout << "Enter instance name: ";
                        if (not (std::cin >> sight.name)) {
                            std::cerr << "Error: Could not get name from stdin\n";
                            continue;
                        }
                    } else {
						std::cerr << "Usage: set name [new_name]\n";
						continue;
					}
                } else if (arg(0) == "hp") {
                    if (num_args == 2) {
                        sight.set_hp(Angle::minutes(std::stod(arg(1))));
                    } else if (num_args == 1) {
                        std::cout << "Enter horizontal parallax [']: ";
                        std::string hp_str;
                        if (std::cin >> hp_str) {
                            sight.set_hp(Angle::minutes(std::stod(hp_str)));
                        } else {
                            std::cerr << "Error: Could not get hp from stdin\n";
                            continue;
                        }
                    } else {
						std::cerr << "Usage: set hp [horizontal parallax [']]\n";
						continue;
					}
                } else if (arg(0) == "sd") {
                    if (num_args == 2) {
                        sight.set_sd(Angle::minutes(std::stod(arg(1))));
                    } else if (num_args == 1) {
                        std::cout << "Enter semidiameter [']: ";
                        std::string sd_str;
                        if (std::cin >> sd_str) {
                            sight.set_sd(Angle::minutes(std::stod(sd_str)));
                        } else {
                            std::cerr << "Error: Could not get sd from stdin\n";
                            continue;
                        }
                    } else {
						std::cerr << "Usage: set sd [semidiameter [']]\n";
						continue;
					}
                } else if (arg(0) == "hoe") {
                    if (num_args == 2) {
                        sight.environment.height_of_eye = std::stod(arg(1));
                    } else if (num_args == 1) {
                        std::cout << "Enter height of eye [m]: ";
                        std::string hoe;
                        if (std::cin >> hoe) {
                            sight.environment.height_of_eye = std::stod(hoe);
                        } else {
                            std::cerr << "Error: Could not get HoE from stdin\n";
                            continue;
                        }
                    } else {
						std::cerr << "Usage: set hoe [height of eye [m]]\n";
						continue;
					}
                } else if (arg(0) == "temp") {
                    if (num_args == 2) {
                        sight.environment.temp_kelvin = celsius_to_kelvin(std::stod(arg(1)));
                    } else if (num_args == 1) {
                        std::cout << "Enter air temperature [°C]: ";
                        std::string temp_str;
                        if (std::cin >> temp_str) {
                            sight.environment.temp_kelvin = celsius_to_kelvin(std::stod(temp_str));
                        } else {
                            std::cerr << "Error: Could not get air temperature from stdin\n";
                            continue;
                        }
                    } else {
						std::cerr << "Usage: set temp [temperature [°C]]\n";
						continue;
					}
                } else if (arg(0) == "pres") {
                    if (num_args == 2) {
                        sight.environment.pres_hpascal = std::stod(arg(1));
                    } else if (num_args == 1) {
                        std::cout << "Enter air pressure [hPa]: ";
                        std::string pres_str;
                        if (std::cin >> pres_str) {
                            sight.environment.pres_hpascal = std::stod(pres_str);
                        } else {
                            std::cerr << "Error: Could not get air pressure from stdin\n";
                            continue;
                        }
                    } else {
						std::cerr << "Usage: set pres [pressure [hPa]]\n";
						continue;
					}
                } else if (arg(0) == "ie") {
                    if (num_args == 2) {
                        sight.environment.index_error = Angle::minutes(std::stod(arg(1)));
                    } else if (num_args == 1) {
                        std::cout << "Enter index error [']: ";
                        std::string ie_str;
                        if (std::cin >> ie_str) {
                            sight.environment.index_error = Angle::minutes(std::stod(ie_str));
                        } else {
                            std::cerr << "Error: Could not get index error from stdin\n";
                            continue;
                        }
                    } else {
						std::cerr << "Usage: set ie [index error [']]\n";
						continue;
					}
                } else if (arg(0) == "body") {
                    if (num_args == 2) {
                        if (auto const body = body_from_string(arg(1))) {
                            sight.set_body(*body);
                        } else {
                            std::cerr << "Usage: set body [sun|star|planet|moon]\n";
                            continue;
                        }
                    } else if (num_args == 1) {
                        std::cerr << "TODO\n";
                        continue;
                    } else {
						std::cerr << "Usage: set body [sun|star|planet|moon]\n";
                        continue;
                    }
				} else {
                    std::cerr << "Unknown parameter: " << arg(0) << "\n";
                }
			} else if (command[0] == 'l') {
				for (size_t i = 0; i < sight.sights.size(); ++i) {
					auto const& single_sight = _cur_instance->sight.sights[i];
					std::cout << i << ": " << to_string(single_sight.limb) << " "
					          << single_sight.sextant_alt.to_string() << " at "
					          << std::format(
                                      "{:%F %H:%M:%S}",
                                      std::chrono::round<std::chrono::duration<long, std::ratio<1,10>>>(
                                          single_sight.time
                                      )
                                 )
                              << "\n";
				}
			} else if (command[0] == 'd') {
				if (_tokens.size() < 2) {
					std::cerr << "Error: Please give LOP number\n";
					continue;
				}
				auto const i = std::stoul(arg(0));
				if (i < sight.sights.size()) {
					sight.sights.erase(sight.sights.begin() + i);
				} else {
					std::cerr << "Error: single sight " << i << " doesn't exist!\n";
				}
			} else if (command[0] == 'a') {
				std::cout << "Adding single sextant sight ...\n";
				SingleSight single_sight;

				if (_tokens.size() > 1) {
					if (auto const maybe_time = time_from(sight.date, arg(0))) {
						single_sight.time = *maybe_time;
					} else {
						continue;
					}
				} else {
					std::cout << " Time [hh:mm:ss]: ";
					single_sight.time = time_from_stdin(sight.date);
				}

				if (_tokens.size() > 2) {
					if (auto const maybe_angle = Angle::from_string(arg(1))) {
						single_sight.sextant_alt = *maybe_angle;
					} else {
						continue;
					}
				} else {
					std::cout << " Measured altitude [deg°[min'[sec\"]]]: ";
					single_sight.sextant_alt = Angle::from_stdin();
				}

				if (_tokens.size() > 3) {
					if (auto const maybe_limb = limb_from(arg(2))) {
						single_sight.limb = *maybe_limb;
					} else {
						continue;
					}
				} else {
					std::cout << " Limb [l|c|u]: ";
					single_sight.limb = limb_from_stdin();
				}
				sight.sights.push_back(single_sight);
			} else {
				std::cerr << "Error: Unknown command: " << buf << ". Type \"help\" for a list of commands.\n";
			}
		} else {
			if (command[0] == 'h') {
				std::cout << "Commands:\n";
				std::cout << " l[ist]: list all LOPs\n";
				std::cout << " s[elect] [i]: select LOP number i (leave empty to deselect)\n";
				std::cout << " a[dd] lop_name: add LOP\n";
				std::cout << " c[opy] i: copy LOP number i and add to back of list\n";
				std::cout << " d[elete] i: remove LOP number\n";
			} else if (command[0] == 'l') {
				for (size_t i = 0; i < _system.instances().size(); ++i) {
					auto const& instance = _system.instances()[i];
					std::cout << i << ": " << instance.one_line_description() << "\n";
				}
			} else if (command[0] == 's') {
				select();
			} else if (command[0] == 'a') {
				if (_tokens.size() < 2) {
					std::cerr << "Error: Please give LOP name\n";
					continue;
				}
				_cur_instance = &_system.add_instance(std::string(arg(0)));
			} else if (command[0] == 'c') {
				if (_tokens.size() < 2) {
					std::cerr << "Error: Please give LOP number\n";
					continue;
				}
				auto const i = std::stoul(arg(0));
				if (i < _system.instances().size()) {
					_cur_instance = &_system.copy_instance(i);
				} else {
					std::cerr << "Error: LOP " << i << " doesn't exist!\n";
				}
			} else if (command[0] == 'd') {
				if (_tokens.size() < 2) {
					std::cerr << "Error: Please give LOP number\n";
					continue;
				}
				auto const i = std::stoul(arg(0));
				if (i < _system.instances().size()) {
					_cur_instance = nullptr;
					_system.remove_instance(i);
				} else {
					std::cerr << "Error: LOP " << i << " doesn't exist!\n";
				}
			} else {
				std::cerr << "Error: Unknown command: " << buf << ". Type \"help\" for a list of commands.\n";
			}
		}
	}
}

void CelNavShell::select()
{
	if (_tokens.size() < 2) {
		_cur_instance = nullptr;
	} else {
		auto const i = std::stoul(arg(0));
		if (i < _system.instances().size()) {
			_cur_instance = &_system.non_const_instance(i);
		} else {
			std::cerr << "Error: LOP " << i << " doesn't exist!\n";
		}
	}
}

} // namespace celnav
