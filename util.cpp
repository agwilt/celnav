#include "util.hpp"
#include <chrono>
#include <iostream>
#include <cmath>

namespace celnav {

TimePoint date_from_stdin()
{
	std::string line;
	while (true) {
		std::getline(std::cin, line);
		if (auto const maybe_date = date_from(line)) {
			return *maybe_date;
		}
	}
}

std::optional<TimePoint> date_from(std::string const& utc_date)
{
	int y,m,d;
	if (std::sscanf(utc_date.c_str(), "%4d-%2d-%2d", &y, &m, &d) != 3) {
		std::cerr << "Please give date in format \"yyyy-mm-dd\"\n";
		return std::nullopt;
	}
	if (y < 1970) {
		std::cerr << "Dates before 1970 not supported\n";
		return std::nullopt;
	}
	return TimePoint{} + std::chrono::years(y - 1970) + std::chrono::months(m) + std::chrono::days(d);
}

TimePoint time_from_stdin(TimePoint const& date)
{
	std::string line;
	while (true) {
		std::getline(std::cin, line);
		if (auto const maybe_time = time_from(date, line)) {
			return *maybe_time;
		}
	}
}

std::optional<TimePoint> time_from(TimePoint const& date, std::string const& time_string)
{
	int h,m;
	double s;
	if (std::sscanf(time_string.c_str(), "%2d:%2d:%5lf", &h, &m, &s) != 3) {
		std::cerr << "Please give time in format \"hh:mm:ss\"\n";
		return std::nullopt;
	}
	return date + std::chrono::hours(h) + std::chrono::minutes(m)
	       + std::chrono::milliseconds(std::lround(s * 1000));
}

Pos pos_from_stdin()
{
	std::string lat, lon;
	while (true) {
		std::cout << "Latitude N/S: ";
		std::getline(std::cin, lat);
		std::cout << "Longitude E/W: ";
		std::getline(std::cin, lon);
		if (auto const maybe_pos = pos_from(lat, lon)) {
			return *maybe_pos;
		} else {
			std::cerr << "Error: Invalid latitude or longitude\n";
		}
	}
}

std::optional<Pos> pos_from(std::string const& lat_string, std::string const& lon_string)
{
	if (lat_string.empty() or lon_string.empty()) {
		std::cerr << "Please supply latitude and longitude\n";
		return std::nullopt;
	}
	Pos ans;

	if (auto const lat = Angle::from_string(lat_string)) {
		switch (lat_string.back()) {
			case 'N':
				ans.lat = *lat;
				break;
			case 'S': 
				ans.lat = -*lat;
				break;
			default:
				return std::nullopt;
		}
	} else {
		return std::nullopt;
	}

	if (auto const lon = Angle::from_string(lon_string)) {
		switch (lon_string.back()) {
			case 'E':
				ans.lon = *lon;
				break;
			case 'W': 
				ans.lon = -*lon;
				break;
			default:
				return std::nullopt;
		}
	} else {
		return std::nullopt;
	}

	return ans;
}

Limb limb_from_stdin()
{
	std::string s;
	while (true) {
		std::cin >> s;
		if (auto const maybe_limb = limb_from(s)) {
			return *maybe_limb;
		}
	}
}

std::optional<Limb> limb_from(std::string const& s)
{
	switch (s[0]) {
		case 'l':
			return Limb::LOWER;
		case 'c':
			return Limb::CENTRE;
		case 'u':
			return Limb::UPPER;
		default:
			std::cerr << "Please give limb l[ower], c[entre] or u[pper]\n";
			return std::nullopt;
	}
}

} // namespace celnav
