#include "Sight.hpp"
#include "util.hpp"

#include <chrono>

namespace celnav {

Sight::Sight() : name{}, sights{}, environment{}, _body{Body::NONE}, _hp{}, _sd{}
{
	date = std::chrono::floor<std::chrono::days>(TimePoint::clock::now());
}

void Sight::set_body(Body const body)
{
	_body = body;
	_hp.reset();
	_sd.reset();
	switch (body) {
		case Body::SUN:
			_hp.emplace(Angle::minutes(0.15));
			break;
		case Body::STAR:
			_hp.emplace(0_deg);
			_sd.emplace(0_deg);
			break;
		case Body::PLANET:
			_sd.emplace(0_deg);
			break;
		case Body::MOON:
			break;
		default: exit(1); // invalid body
	}
}

void Sight::set_hp(Angle const hp_radians)
{
	assert(_body == Body::MOON or _body == Body::PLANET);
	_hp = hp_radians;
}

void Sight::set_sd(Angle const sd_radians)
{
	assert(_body == Body::MOON or _body == Body::SUN);
	_sd = sd_radians;
}

void Sight::set_gp(Pos const gp)
{
    _geographic_position = gp;
}

std::optional<LineOfPosition> Sight::process() const
{
	if (sights.empty()) {
		return std::nullopt;
	}
	if (not _hp.has_value()) {
		return std::nullopt;
	}
	if (not _sd.has_value()) {
		return std::nullopt;
	}
	if (not _geographic_position.has_value()) {
		return std::nullopt;
	}

	auto observed_altitude = 0_deg;
	auto avg_time_summand = sights.front().time - sights.front().time;
	// next step: best-fit parabola?
	// TODO: numerically better averaging
	for (auto const& sight : sights) {
		observed_altitude += environment.alt_corr(sight.sextant_alt, sight.limb, *_hp, *_sd);
		avg_time_summand += (sight.time - sights.front().time);
	}
	observed_altitude /= sights.size();
	avg_time_summand /= sights.size();

	return LineOfPosition{*_geographic_position, observed_altitude, sights.front().time + avg_time_summand};
}

}
