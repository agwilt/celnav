CC = clang
CXX = clang++
#CCFLAGS = -Wall -Wextra -O3 -pedantic -march=native -mtune=native -g -pg
CCFLAGS = -Wall -Wextra -O3 -pedantic -march=native -mtune=native
CXXFLAGS = -std=c++20 $(CCFLAGS)

DEPS = Makefile base.hpp
OBJ = obj

$(OBJ)/%.o: %.c %.h $(DEPS)
	mkdir -p $(OBJ)
	$(CC) -c -o $@ $< $(CCFLAGS)

$(OBJ)/%.o: %.cpp %.hpp $(DEPS)
	mkdir -p $(OBJ)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

all: celnav

celnav: celnav.cpp $(OBJ)/Environment.o $(OBJ) $(OBJ)/Instance.o $(OBJ)/Pos.o $(OBJ)/Sight.o $(OBJ)/System.o $(OBJ)/CelNavShell.o $(OBJ)/util.o $(OBJ)/Angle.o
	$(CXX) $(CXXFLAGS) -lreadline celnav.cpp $(OBJ)/{Environment,Instance,Pos,Sight,System,CelNavShell,util,Angle}.o -o celnav

.PHONY: clean obj_clean bin_clean
clean: obj_clean bin_clean
bin_clean:
	rm -fv celnav
obj_clean:
	rm -rfv $(OBJ)/*
