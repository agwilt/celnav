#ifndef SIGHT_H
#define SIGHT_H

#include <vector>
#include <optional>
#include <string>

#include "base.hpp"
#include "LineOfPosition.hpp"
#include "Pos.hpp"
#include "Environment.hpp"

namespace celnav {

struct SingleSight {
	Limb limb;
	Angle sextant_alt;
	TimePoint time;
};

class Sight {
public:
	Sight();

	std::optional<LineOfPosition> process() const; // returns std::nullopt if not enough data

	void set_body(Body body);
	void set_hp(Angle hp_radians);
	void set_sd(Angle sd_radians);
	void set_gp(Pos geographic_position);

    constexpr Body body() const { return _body; }
    constexpr std::optional<Angle> const& hp() const { return _hp; }
    constexpr std::optional<Angle> const& sd() const { return _sd; }
	constexpr std::optional<Pos> const& geographic_position() const { return _geographic_position; }

public:
	std::string name;
	std::vector<SingleSight> sights;
	Environment environment;
	TimePoint date;

private:
	Body _body; // we've hidden the body!
	std::optional<Angle> _hp; // horizontal parallax in radians
	std::optional<Angle> _sd; // semidiameter in radians
	std::optional<Pos> _geographic_position;
};

}

#endif // SIGHT_H
