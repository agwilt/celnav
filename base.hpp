#ifndef CELNAV_BASE_H
#define CELNAV_BASE_H

#include <chrono>

namespace celnav {

enum class Body {
	NONE,
	SUN,
	STAR,
	PLANET,
	MOON,
    NUM_BODIES
};

enum class Limb {
	LOWER,
	CENTRE,
	UPPER
};

using TimePoint = std::chrono::time_point<std::chrono::system_clock>;

constexpr std::string to_string(celnav::Body const body)
{
	switch (body) {
		case Body::NONE: return "none";
		case Body::SUN: return "sun";
		case Body::STAR: return "star";
		case Body::PLANET: return "planet";
		case Body::MOON: return "moon";
        default: exit(1);
	}
}
constexpr std::optional<Body> body_from_string(std::string const& body_string)
{
    for (auto body = Body::NONE; body != Body::NUM_BODIES; body = Body(static_cast<int>(body) + 1)) {
        if (to_string(body) == body_string) {
            return body;
        }
    }
    return std::nullopt;
}

constexpr std::string to_string(celnav::Limb const limb)
{
	switch (limb) {
		case celnav::Limb::LOWER: return "lower limb";
		case celnav::Limb::CENTRE: return "centre";
		case celnav::Limb::UPPER: return "upper limb";
	}
}

} // namespace celnav

#endif // CELNAV_BASE_H
